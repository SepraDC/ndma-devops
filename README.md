### NDMA DEVOPS

Pour déployer le projet suivez les étapes suivantes.

Obtenir un .war du projet
>```mvn package```

Copiez ensuite le .war situé dans le dossier **target** dans le dossier **Docker/Tomcat**

Ensuite nous allons build le container tomcat.
Pour cela en ligne de commande rendez vous dans le dossier **Docker** puis tapez la commande suivante
>``docker build -t ndma/tomcat Tomcat``

Une fois que l'image est build nous allons build l'image Mysql pour cela toujours dans le dossier **Docker** tapez la commande suivante
>``docker build -t ndma/mysql MySQL``

puis une fois que les deux images sont build il ne reste plus qu'a lancer le fichier **docker-compose.yml** avec la commande suivante
>``docker-compose up``

Si tout s'est bien passé vous devriez pouvoir acceder à l'url de votre container tomcat.
> [http://localhost:8080/ndma-devops](http://localhost:8080/ndma-devops/)