drop table if exists Recipe_Ingredient;
drop table if exists Step;
drop table if exists Ingredient;
drop table if exists Recipe;

CREATE TABLE Recipe(
  id INT PRIMARY KEY NOT NULL auto_increment,
  name VARCHAR(50) NOT NULL,
  personNumber INT NOT NULL,
  preparationTime DECIMAL NOT NULL,
  cookingTime DECIMAL NOT NULL,
  difficulty INT NOT NULL
);

CREATE TABLE Step(
  id INT PRIMARY KEY NOT NULL auto_increment,
  `order` INT NOT NULL,
  `text` TEXT NOT NULL,
  recipe_id INT NOT NULL,
  CONSTRAINT fk_recipe_step FOREIGN KEY (recipe_id) REFERENCES Recipe(id)
);

CREATE TABLE Ingredient(
  id INT NOT NULL PRIMARY KEY auto_increment,
  name VARCHAR(50) NOT NULL
);

CREATE TABLE Recipe_Ingredient(
    id INT NOT NULL PRIMARY KEY auto_increment,
    quantity DECIMAL NOT NULL,
    recipe_id INT NOT NULL,
    ingredient_id INT NOT NULL,
    CONSTRAINT fk_recipe_RI FOREIGN KEY (recipe_id) REFERENCES Recipe(id),
    CONSTRAINT fk_ingredient_RI FOREIGN KEY (ingredient_id) REFERENCES Ingredient(id)
);

insert into Recipe(name, personnumber, preparationtime, cookingtime, difficulty) values ('Tarte à la tomate', 4, 10,30,1);
insert into Ingredient(name) VALUES ('Pâte feuilletée'),('Fromage à raclette'),('Tomate'),('Moutarde'),('Herbes de Provence');

insert into Recipe_Ingredient(quantity, recipe_id, ingredient_id)
VALUES (1,(SELECT id FROM Recipe WHERE name = 'Tarte à la tomate'), (SELECT id FROM Ingredient WHERE name = 'Pâte feuilletée')),
       (8,(SELECT id FROM Recipe WHERE name = 'Tarte à la tomate'), (SELECT id FROM Ingredient WHERE name = 'Fromage à raclette')),
       (8,(SELECT id FROM Recipe WHERE name = 'Tarte à la tomate'), (SELECT id FROM Ingredient WHERE name = 'Tomate')),
       (1,(SELECT id FROM Recipe WHERE name = 'Tarte à la tomate'), (SELECT id FROM Ingredient WHERE name = 'Moutarde')),
       (1,(SELECT id FROM Recipe WHERE name = 'Tarte à la tomate'), (SELECT id FROM Ingredient WHERE name = 'Herbes de Provence'));

insert into Step(`order`, `text`, recipe_id)
VALUES (1,'Disposer la pâte dans un moule à tarte.',(SELECT id FROM Recipe WHERE name = 'Tarte à la tomate')),
       (2,'Etaler la moutarde en couche fine sur toute la surface de la pâte.',(SELECT id FROM Recipe WHERE name = 'Tarte à la tomate')),
       (3,'Ajouter les tranches de raclette afin que cela tapisse entierement le moule.',(SELECT id FROM Recipe WHERE name = 'Tarte à la tomate')),
       (4,'Plonger les tomates dans l''eau bouillante pendant moins d''1 minute dans l''eau afin de retirer la peau plus facilement puis les couper en rondelles très fines. Les disposer dans le plat.',(SELECT id FROM Recipe WHERE name = 'Tarte à la tomate')),
       (5,'Ajouter les herbes de Provence.',(SELECT id FROM Recipe WHERE name = 'Tarte à la tomate')),
       (6,'Cuire à four chaud (170°C) pendant 30 minutes environ.',(SELECT id FROM Recipe WHERE name = 'Tarte à la tomate'));
