package com.epsi.b3.ndmadevops.service;

import com.epsi.b3.ndmadevops.model.RecipeIngredient;
import com.epsi.b3.ndmadevops.model.Step;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class RecipeDto {
    @NotBlank
    private String name;

    @NotBlank
    private int personNumber;

    @NotNull
    private float preparationTime;

    @NotNull
    private float cookingTime;

    @Size(min = 1, max = 5, message = "Choisissez une note entre 1 et 5")
    private int difficulty;

    @NotEmpty(message = "Vous devez saisir des étapes")
    private List<Step> steps = new ArrayList<>();

    @NotEmpty(message = "Vous devez saisir des ingrédients")
    private Set<RecipeIngredient> recipeIngredients;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPersonNumber() {
        return personNumber;
    }

    public void setPersonNumber(int personNumber) {
        this.personNumber = personNumber;
    }

    public float getPreparationTime() {
        return preparationTime;
    }

    public void setPreparationTime(float preparationTime) {
        this.preparationTime = preparationTime;
    }

    public float getCookingTime() {
        return cookingTime;
    }

    public void setCookingTime(float cookingTime) {
        this.cookingTime = cookingTime;
    }

    public int getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(int difficulty) {
        this.difficulty = difficulty;
    }

    public List<Step> getSteps() {
        return steps;
    }

    public void setSteps(List<Step> steps) {
        this.steps = steps;
    }

    public Set<RecipeIngredient> getRecipeIngredients() {
        return recipeIngredients;
    }

    public void setRecipeIngredients(Set<RecipeIngredient> recipeIngredients) {
        this.recipeIngredients = recipeIngredients;
    }
}
