package com.epsi.b3.ndmadevops.service;

import com.epsi.b3.ndmadevops.model.Recipe;
import com.epsi.b3.ndmadevops.repository.RecipeRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class RecipeService {
    @Autowired
    private RecipeRepository recipeRepository;

    public List<Recipe> getRecipeList(){return recipeRepository.getRecipeList();}

    public Recipe getRecipeByName(String name) throws Exception {
        return recipeRepository.getRecipeByName(name);
    }

    @Transactional
    public Recipe create(RecipeDto recipeDto) {
        Recipe recipe = new Recipe();
        recipe.setName(recipeDto.getName());
        recipe.setCookingTime(recipeDto.getCookingTime());
        recipe.setPreparationTime(recipeDto.getPreparationTime());
        recipe.setPersonsNumber(recipeDto.getPersonNumber());
        recipe.setRecipeIngredients(recipeDto.getRecipeIngredients());
        recipe.setSteps(recipeDto.getSteps());

        return recipeRepository.create(recipe);
    }
}
