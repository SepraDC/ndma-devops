package com.epsi.b3.ndmadevops.service;

import com.epsi.b3.ndmadevops.model.Ingredient;
import com.epsi.b3.ndmadevops.repository.IngredientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class IngredientService {
    @Autowired
    private IngredientRepository ingredientRepository;

    public List<Ingredient> getIngredientList(){
        return this.ingredientRepository.getIngredientList();
    }
}
