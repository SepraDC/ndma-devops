package com.epsi.b3.ndmadevops.repository;

import com.epsi.b3.ndmadevops.model.Ingredient;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class IngredientRepository {
    @PersistenceContext
    private EntityManager em;

    public List<Ingredient> getIngredientList(){
        return em.createQuery("select i from Ingredient i", Ingredient.class)
                .getResultList();
    }
}
