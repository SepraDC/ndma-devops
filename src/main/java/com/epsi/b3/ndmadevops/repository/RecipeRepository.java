package com.epsi.b3.ndmadevops.repository;

import com.epsi.b3.ndmadevops.model.Recipe;
import javassist.NotFoundException;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.HttpClientErrorException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class RecipeRepository {
    @PersistenceContext
    private EntityManager em;

    public List<Recipe> getRecipeList(){
        return em.createQuery("select r from Recipe r", Recipe.class)
                .getResultList();
    }

    public Recipe getRecipeById(Long id){
        return em.find(Recipe.class, id);
    }

    public Recipe getRecipeByName(String name) throws Exception {
        List<Recipe> recipes = em.createQuery("select r from Recipe r where r.name = :name", Recipe.class)
                .setParameter("name", name)
                .setMaxResults(1)
                .getResultList();
        if(recipes.isEmpty()){
            throw new Exception("Pas de resultats");
        }
        return recipes.get(0);
    }

    public Recipe create(Recipe recipe) {
        em.persist(recipe);
        return recipe;
    }
}
