package com.epsi.b3.ndmadevops.controller;

import com.epsi.b3.ndmadevops.model.Recipe;
import com.epsi.b3.ndmadevops.service.IngredientService;
import com.epsi.b3.ndmadevops.service.RecipeDto;
import com.epsi.b3.ndmadevops.service.RecipeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class RecipeController {
    @Autowired
    private RecipeService recipeService;

    @Autowired
    private IngredientService ingredientService;

    @GetMapping("/")
    public String home(Model model){
        model.addAttribute("recipes", recipeService.getRecipeList());
        return "accueil";
    }

    @GetMapping("/details")
    public String details(Model model, @RequestParam String name) throws Exception {
        model.addAttribute("recipe", recipeService.getRecipeByName(name));
        return "details";
    }

    @GetMapping("/create")
    public String recipeForm(Model model){
        model.addAttribute("ingredients", ingredientService.getIngredientList());


        return "create-recipe";
    }

    @PostMapping("/create")
    public String createRecipe(Model model, @Validated @ModelAttribute RecipeDto recipeDto, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return recipeForm(model);
        }
        Recipe recipe = recipeService.create(recipeDto);
        return "redirect:/details?name=" + recipe.getName();
    }


}
