<%--
  Created by IntelliJ IDEA.
  User: SepraDC
  Date: 15/07/2020
  Time: 10:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
    <title>Bonjour</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
</head>
<body>
<div class="container mt-2">
    <div class="d-flex justify-content-between">
        <h1>Liste des recettes disponibles</h1><a class="btn btn-primary m-2" href="<c:url value="/create"/>">Ajouter une recette</a>
    </div>

    <c:forEach items="${recipes}" var="r">
        <div class="card" style="width: 18rem;">
            <div class="card-body">
                <h5 class="card-title"><c:out value="${r.name}"/></h5>
                <p class="card-text">
                    <span><c:out value="${r.personsNumber}"/> personnes</span><br>
                    <span> Preparation <c:out value="${r.preparationTime}"/> min</span><br>
                    <span> Cuisson <c:out value="${r.cookingTime}"/> min</span><br>
                    <span> Difficulté <c:out value="${r.difficulty}"/></span>
                </p>
                <a class="btn btn-primary btn-lg btn-block" href="<c:url value="/details"/>?name=<c:out value="${r.name}"/>">Miam</a>
            </div>
        </div>
    </c:forEach>
</div>
</body>
</html>
