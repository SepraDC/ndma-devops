<%--
  Created by IntelliJ IDEA.
  User: SepraDC
  Date: 16/07/2020
  Time: 11:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
    <title><c:out value="${recipe.name}"/></title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
</head>
<body>
<div class="container p-2">
<h1><c:out value="${recipe.name}"/></h1>
<span><c:out value="${recipe.personsNumber}"/> personnes</span>
<span>Préparation <c:out value="${recipe.preparationTime}"/> min</span>
<span>Cuisson <c:out value="${recipe.cookingTime}"/> min</span>
<c:forEach items="${recipe.recipeIngredients}" var="i">
    <br>
    <c:out value="${i.quantity}"/> <c:out value="${i.ingredient.name}"/>
</c:forEach>
<c:forEach items="${recipe.steps}" var="s">
    <li><c:out value="${s.text}"/></li>
</c:forEach>
</div>

</body>
</html>
